\documentclass[A4]{particles2017}

\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\newcommand\norm[1]{\left\lVert#1\right\rVert}
\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{etoolbox}
\usepackage{setspace}
\usepackage{xcolor}


\title{A Language and Development Environment for Parallel Particle Methods}

\author{Sven Karol$^{1,4}$, Tobias Nett$^1$, Pietro Incardona$^{2,3}$, Nesrine Khouzami$^1$, Jeronimo Castrillon$^1$, Ivo F. Sbalzarini$^{2,3}$}

\heading{S. Karol, T. Nett, P. Incardona, N. Khouzami, J. Castrillon and I. F. Sbalzarini}

\address{$^{1}$ Chair for Compiler Construction\\
 Center for Advancing Electronics Dresden, TU Dresden, Dresden, Germany\\
{[}tobias.nett|sven.karol|nesrine.khouzami|jeronimo.castrillon{]}@tu-dresden.de
\and
$^{2}$ Chair of Scientific Computing for Systems Biology, Faculty\\
of Computer Science, TU Dresden, Dresden, Germany
\and
$^{3}$ MOSAIC Group, Center for Systems Biology Dresden,\\
Max Planck Institute of Molecular Cell Biology and Genetics, Dresden, Germany\\
{[}incardon@mpi-cbg.de|ivos{]}@mpi-cbg.de
\and
$^{4}$ Baselabs GmbH, Ebertstr. 10, 09126 Chemnitz, Germany}

\keywords{Particles Method, discrete element method, Gray-Scott, Lennard-Jones, PPME, PPML, DSL}

\abstract{We present the Parallel Particle-Mesh Environment (PPME), a domain-specific language (DSL) and
development environment for numerical simulations using particles and hybrid particle-mesh
methods. PPME is the successor of the Parallel Particle-Mesh Language (PPML), a Fortran-based DSL that provides high-level abstractions for the development of distributed-memory particle-mesh simulations. On top 
of PPML, PPME provides a complete development environment for particle-based simulations usin state-of-the-art language engineering and compiler construction
techniques. Relying on a novel domain metamodel and formal type system for particle methods,
it enables advanced static code correctness checks at the level of particle abstractions, 
complementing the low-level analysis of the compiler. Furthermore, PPME adopts Herbie for 
improving the accuracy of floating-point expressions and supports a convenient 
high-level mathematical notation for equations and differential operators. For demonstration purposes, 
we discuss an example from Discrete Element Methods (DEM) using the classic Silbert model to simulate granular flows.}

\begin{document}
%\maketitle
\section{INTRODUCTION}
Developing applications for scientific high-performance computing (HPC) requires in-depth knowledge 
of the underlying, potentially heterogeneous hardware and programming models, as well as the corresponding numerical 
simulation methods and parallel programming patterns. This leads to a low level of achievable abstraction, which is 
 known to cause the ``knowledge gap'' in scientific programming~\cite{Sbalzarini:2010}. To address this gap, scientific libraries and domain-specific languages (DSLs) have evolved into an important toolset in scientific HPC. In the domain of 
particle methods, this notably includes the Parallel Particle Mesh library (PPM)~\cite{Sbalzarini2006a,Sbalzarini:2010,Awile:2010a} and the Parallel Particle Mesh Language
(PPML)~\cite{Awile:2013,Awile:2013a} as a library and a DSL for large-scale scientific HPC.
The abstractions in PPM and PPML allow scientific programmers to write more concise and declarative code in
comparison to hand-coded implementations. Essentially, it frees developers from the burden of
writing boilerplate code that manages parallelism, synchronization, and data distribution. However,
PPML has downsides, which we address in PPME~\cite{Nett2017}: The lightweight embedding of PPML into
Fortran, based on language macros, and the lack of a fully integrated language model 
prevent advanced code analysis and complex compile-time computations. This makes debugging PPML programs hard and prohibits domain-specific static code optimization~\cite{Karol2015a}. In contrast, PPME closely follows the paradigm of
language-oriented programming~\cite{Ward1994}, where extensible DSLs are created to describe and solve software problems instead of writing programs in general-purpose languages. This serves to increase maintainability and productivity through domain abstractions. 
PPME is integrated into the meta programming system (MPS)~\cite{Dmitriev2004,MPS32UsersGuide}, a model-driven 
language workbench~\cite{fowler_language_workbenches_2005}. We developed a language model that enables 
us to implement analysis and optimization algorithms that are well-suited for particle methods. The model is the basis
of a formal type system for particle simulations, including optional verification of physical dimensions. This enables advanced
domain-specific correctness checks at compile time, such as checking for dimensional correctness. PPME further supports numerical accuracy optimization capabilities of floating-point expressions by leveraging domain knowledge and adopting the Herbie accuracy checker~\cite{Panchekha2015}. Due to 
MPS' projectional editing capabilities, convenient high-level mathematical notation for equations and differential operators is supported.
In this paper, we present PPME and show its use in Discrete Element Methods (DEM). 
In particular, we use PPME to implement a simulation of granular flows on distributed-memory parallel computers using a classical Herzian contact force model.

The remainder of this paper is structured as follows: Section~\ref{sec:related_work} discusses related work. Section~\ref{sec:ppme} presents the architecture of PPME and its integration with the PPM Library. Section~\ref{sec:case_study} introduces our case study from the domain of discrete element methods while its implementation in PPME and  
results are discussed in Section~\ref{sec:results}. Section~\ref{sec:conclusions} concludes the paper and gives an outlook of future work.

\section{RELATED WORK}
\label{sec:related_work}
% DSLs in scientific computing
%During the last years, the importance of DSLs for scientific computing has been increasingly 
%realized. This led to the emergence of a number of approaches of which
%we mention a few notable examples. 
%
In scientific computing, several DSL-like approaches have successfully been proposed in the past: 
%
Blitz++~\cite{veldhuizen_blitz_2000} is a
template-based library and DSL for generating stencils
from high-level mathematical specifications of mesh-based computations.
%
 Freefem++~\cite{hecht_freefem_2012} is a
software toolset and DSL for finite-element methods. This DSL allows
users to define analytic or finite-element functions using domain
abstractions such as meshes and differential operators.
%
Liszt~\cite{devito_liszt_2011} extends Scala with domain-specific statements
for defining solvers for partial differential equations on unstructured meshes with support for several
parallel programming models including message passing. 
%
The
FEniCS project~\cite{logg_automated_2012} created a finite element library, the
unified form language (UFL)~\cite{alnaes_ufl_2014}, and several optimizing compilers
for generating code that can be used with the library.
Firedrake~\cite{rathgeber_firedrake_2015} adds composing abstractions such as
parallel loop operations.
%Similarly, in the future, we plan to develop a set of particle-method related optimizations 
%on top of PPME. For mesh-based discretizations, it would be an opportunity to integrate 
%the UFL, providing a unified framework for both abstractions.
%IFS: I would not mention this. I also think UFL might not be good because it is centered on bilinear forms and weak discretizations, which you only get in FEM

% DSL optimizations
%In many cases, it suffices to translate the DSL code into a lower-level target language like
%C and let its compiler apply standard optimization. 
%Otherwise, rules for optimizations may be programmed or realized using some of the approaches discussed
%previously.
%The idea of transforming or rewriting program code for optimization purposes is not new. 
%For example, a DSL optimizer could be implemented using program
%transformations~\cite{schordan_user_optimizations_2003} or rewrite rules. However,
%research on using graph-rewrite systems for such
%tasks~\cite{amann_graph_2000,schoesser_graph_optimizations_2008} indicates that the
%pattern language must be powerful enough to express context-sensitive patterns.
%
%Furthermore, recent research shows that rewriting is a convenient technique for
%implementing high-level optimizations on a restricted set of language constructs. For
%instance, authors in~\cite{Panchekha2015} propose a method for
%automatically improving the accuracy of floating-point expressions by rewriting such
%expressions according to a set of harvested patterns. Further, the authors
%of~\cite{steuwer_rewriting_2015} apply rewriting to specific functional expressions
%for parallel computations to obtain efficient GPU kernels.
%
Domain-specific optimizations carry
great potential since scientific codes often induce specific boundaries on data
access and numeric algorithms. 
%
This has been particularly studied for representation code of element tensors
in the finite-element method~\cite{olgaard_fem_optimization_2010}. The representation code is written in
UFL variational forms. The proposed optimization strategies yield significant runtime speedups and leverage
domain knowledge to automate nontrivial optimizations. 
%
Loop-level optimizations for finite-element solvers in the COmpiler For Fast Expression Evaluation
(COFFEE)~\cite{luporini_coffee_2015} are discussed in Ref.~\cite{luporini_algorithm_2016}. Therein, heuristics are used to predict operation counts at runtime, using well-known transformations such as
code motion, expansion, and factorizations. Domain-specific optimizations are superior to 
 general-purpose ones that are used by standard compilers to reduce the operation count in nested loops. 

This includes checking and optimizing variables based on their physical dimensions. For this, an analysis technique based on unit annotations has been proposed~\cite{Cook2006} that does not require extending or changing the base language. Furthermore, unit annotations 
for linear-algebra and finite-element calculations are available~\cite{Austin2006}, which is comparable to what we have realized for particle methods with dimensional annotations in PPME.
%
%However, adding units to programming languages frequently has flaws. 
%For instance, frameworks may use abstractions with boxing and unboxing of 
%quantities and units, which implies a runtime overhead.
%In our approach, the analysis is optional and does not have an impact on runtime 
%performance, since it is only used at compile-time for consistency checking and does not persist in the %simulation. 


\section{THE PARALLEL PARTICLE-MESH ENVIRONMENT (PPME)}
\label{sec:ppme}
PPME is part of the PPM stack and provides several abstractions and analysis algorithms for parallel particle-mesh methods. The PPM library supports simulations of both discrete and continuous models using either particles, meshes, or a combination thereof. In discrete models, particles directly interact by pairwise kernels. In continuous models, differential operators are discretized on particles using, e.g., the SPH or DC-PSE methods~\cite{Schrader_2010}. In DC-PSE, the discretized kernels are automatically computed at runtime. As PPM is a Fortran library, clients are required to write plain Fortran code in order to use the library. PPML 
partially frees the developer from this burden by providing a collection of macros and code transformations that can be used as high-level abstractions. However, using PPML can be problematic as it does not provide error checking and debugging capabilities so that errors are only detected by the compiler in the generated Fortran code and are not related to the PPML program. PPME addresses this problem by analyzing the user code and providing an extensible infrastructure for incorporating domain-specific optimizations.

\subsection{Architecture of PPME}
%
\begin{figure}[t]
	\centering
	\includegraphics[width=0.6\textwidth]{images/ppme_overview.pdf}
	\caption{Layered architecture of the PPM/PPME stack.}
	\label{fig:ppm_stack}
\end{figure}
%
Figure~\ref{fig:ppm_stack} provides an overview of the current PPM/PPME stack. The architectural details of the computer hardware 
are located on the lowest layer and are accessed through common low-level libraries such as MPI for message passing, PETSc for direct and iterative solvers, METIS~\cite{Karypis:1998b} for graph partitioning, and FFTW for Fourier transforms. In the layer above, the PPM library provides its major subcomponents: \emph{PPM core} contains the distributed  
Fortran data structures and methods for describing particle simulations, while \emph{PPM numerics} provides frequently used numerical algorithms such as multi-grid solvers, spectral solvers, boundary element methods, and fast multipole methods~\cite{Sbalzarini:2006c}. These are partly implemented using the objects provided by the PPM core, and partly based on wrapping external libraries. 
%(cf. \textbf{[WHICH papers?]}{\color{red}{ FIXME}}). 
PPML sits on top of PPM. Based on its abstractions, mixed with plain Fortran for direct library access, PPML provides a high-level domain-specific language that is translated to plain Fortran code code that links against the PPM Library, as well as the underlying libraries~\cite{Awile:2013,Awile:2013a}.
%
Offering a consistent DSL layer, PPME resides in the top-most layer and does not require any adaptation of the underlying framework. Thus, it uses PPML as an intermediate representation, preserving its concepts and abstractions so that the original tool chain remains intact. However, PPME allows scientists to bypass the details of the PPML programming languages and the problem associated with debugging or optimizing PPML programs. PPME is generic to all types of particle and particle-mesh methods, such that different ``client applications'', symbolized by the square boxes on top, can be implemented.

\subsection{Internal Structure of PPME}
Internally, PPME is organized in language packages as illustrated in Figure~\ref{ppme_internal}. These packages correspond to \emph{solutions} in MPS, constituting the domain language model. We briefly describe the packages here. For a more detailed discussion on the model and type system we refer to Ref.~\cite{Nett2017}.
%

The package \texttt{ppme.expressions} provides a domain-independent set of notations for mathematical
and logical expressions, and literals for integer and floating-point numbers. Moreover, the base types available in
PPME and parts of the type system (static analysis) are defined in this package.
%
The package \texttt{ppme.statements} contains universal imperatives, such as expression statements, if-else clauses, and loops.
The type system is enriched with variable support where necessary.
%
The core package \texttt{ppme.core} contains elements that are tailored to particle methods, domain-specific types, expressions, and statements, e.g., a \emph{timeloop} construct.
%
\begin{figure}[t]
	\centering
	\includegraphics[width=0.4\textwidth]{images/ppme_architecture.pdf}
	\caption{Internal structure of PPME and integration into the MPS language workbench.}
	\label{ppme_internal}
\end{figure}%
%
The package \texttt{ppme.modules} provides the top-level structure for client programs written in PPME. 
Modules contain the simulation code and optional control parameters. A module translates to a PPML client that then translates to 
Fortran code.

A major concept of PPME, and of language-oriented programming in general, is to flexibly add language extensions as needed.
So far, we have incorporated two optional extensions:
%
The package \texttt{ppme.phys\-units} enables annotating further meta information to variables and constants.
This includes specifications of physical units and dimension which are then accessible to the type system.
%
The package \texttt{ppme.analysis} provides an exemplary binding of \emph{Herbie} as an external analysis tool for improving
floating-point expressions~\cite{Panchekha2015}. This integration is based on a general framework enabling the 
access of custom tools into the environment.
%
For details on both extensions and application examples, we refer to Ref.~\cite{Nett2017}.

\subsection{Code Generation}
We illustrate PPME by comparing the input and output of the PPME code generator in a simple example: a 
Gray-Scott reaction-diffusion simulation. The two versions of the code, PPML and PPME, are juxtaposed in Figure~\ref{ppme_grayscott_rhs}.
For both, we show the part that integrates the governing equations discretized over the particle set $c$ using the 
4th-order Runge-Kutta method (``rk4''). In PPME, the equations are conveniently 
defined over particle attributes, here the two scalar fields $U$ and $V$. Static analysis extracts the required information from the 
code, for example identifying two applications of differential operators,
{$\nabla^2c\rightarrow U$} and {$\nabla^2c\rightarrow U$}, and automatically deriving and adding local variables $dU$ and $dV$ for them.
%
In the generated PPML code, this information is then contained explicitly, demonstrating some 
key benefits of a holistic code representation and analysis: since all required information 
is extracted by the PPME compiler, redundant statements encoding this information are explicitly 
 avoided, which leads to less code, less compile-time errors, and improved readability. 
Also note PPME's support of basic mathematical notation, such as the Nabla operator $\nabla$ and the partial 
derivative $\partial$.
%
\begin{figure}[t]
	\centering
	\includegraphics[width=1\textwidth]{images/ppme-rhs_comparison.pdf}
	\caption{Equations of a Gray-Scott reaction-diffusion system in PPME (input) and PPML (output).}
	\label{ppme_grayscott_rhs}
\end{figure}%
%
From the initial 4 lines of PPME code, 11 lines of PPML code, and more than 100 lines of Fortran code are generated,
which corresponds to factor of 25 in code-size reduction (cf.~\cite{Nett2017}).

%\section{Case Study: Discrete Element Methods (DEM)}
\section{CASE STUDY: DISCRETE ELEMENT METHODS (DEM)}
\label{sec:case_study}

% SK: Just leave these for the TOMS paper and just refer to them
%\subsection{Gray-Scott Reaction-Diffusion System}
%\subsection{Lennard-Jones Molecular Dynamics}

%\subsection{Discrete Element Methods (DEM)}

Discrete element methods are a fundamental tool for the study of granular materials.
It has been shown that DEM methods allow
determining material properties \cite{Hunt} and effective macroscopic dynamics for which closed-form theory lacks. Therefore, DEM simulations have become invaluable in the search for continuum theoretic
descriptions of granular matter \cite{Gennes,Douady1,Douady2,Douady3}.
In the processing industry, granular materials are center stage, and DEM simulations are widely used to engineer and optimize production and transport processes. DEM simulations, for example, have been used to understand packing in a cylindrical container \cite{Landry} for better silo engineering, and to study concrete structures under load in order to predict failure points and weaknesses \cite{Camborde,Hentz,Shiu}.

Granular materials can be modeled by interacting particles, where each particle is a physical granule or a volume of material \cite{Luding}. The modeled interaction between the particles defines the microscopic behavior of the material. 

A classical model for DEM simulations of spherical granular flows is the Silbert model~\cite{Silbert}. It includes a Herzian contact force, as well as elastic deformation of the grains. Each particle is represented by the location of its center of mass $\vec{r}_i$ and is characterized by its radius $R$, mass $m$, and polar moment of inertia $I$. Whenever two particles $i$ and $j$ collide or are in contact with each other, the radial elastic contact deformation is given by:
%
\begin{equation}
\delta_{ij} = 2R-r_{ij} \, ,
\end{equation}
%
with $\vec{r}_{ij}=\vec{r}_i - \vec{r}_j$ the vector connecting the two 
particle centers and $r_{ij} = \|\vec{r}_{ij}\|_2$ its length.
The normal (radial) and tangential components of the relative velocity at the point of 
contact are given by:
%
\begin{equation}
\vec{v}_{n_{ij}} = \left(\vec{v}_{ij}\cdot\vec{n}_{ij}\right)\vec{n}_{ij} \, ,
\label{eq2}
\end{equation}
\begin{equation}
\vec{v}_{t_{ij}} = \vec{v}_{ij}-\vec{v}_{n_{ij}}-R\left(\vec{\omega}_i +
\vec{\omega}_j \right)\times \vec{n}_{ij} \, ,
\label{eq3}
\end{equation}
%
where $\vec{n}_{ij}=\vec{r}_{ij}/r_{ij}$ is the unit normal vector connecting the two particle centers, $\vec{\omega}_i$ is the angular velocity of particle $i$, and
$\vec{v}_{ij}=\vec{v}_i-\vec{v}_j$ the relative velocity of the two
particles. The  tangential elastic displacement $\vec{u}_{t_{ij}}$ is integrated over time for the duration of contact, using an explicit Euler time-stepping scheme:
%
\begin{equation}
\vec{u}_{t_{ij}} \leftarrow \vec{u}_{t_{ij}} + \vec{v}_{t_{ij}} \delta t
\label{eq:eldist}
\end{equation}
%
with time-step stize $\delta t$. This deformation is stored for each particle and for each contact point. For a new contact, the tangential elastic displacement is initialized to zero. Thus for each unique pair of interacting (colliding) particles, the normal and tangential contact 
forces become:
%
\begin{equation}
\vec{F}_{n_{ij}}=\sqrt{\frac{\delta_{ij}}{2R}}\,\,\left(k_n\delta_{ij}\vec{n}_{ij}-\gamma_n
m_{\text{eff}}\vec{v}_{n_{ij}}\right) \, ,
\label{eq5}
\end{equation}
%
\begin{equation}
\vec{F}_{t_{ij}}=\sqrt{\frac{\delta_{ij}}{2R}}\,\,\left(-k_t\vec{u}_{t_{ij}}-\gamma_t
m_{\text{eff}}\vec{v}_{t_{ij}}\right) \, ,
\end{equation}
%
where $k_{n,t}$ are the elastic constants in the normal and tangential direction,
respectively, and $\gamma _{n,t}$ the corresponding viscoelastic constants. The
effective collision mass is given by $m_{\text{eff}}=\frac{m_i m_j}{m_i+m_j}$. In order to enforce Coulomb's law $\|\vec{F}_{t_{ij}}\|_2 < \|
\mu\vec{F}_{n_{ij}}\|_2$, the tangential force of each contact point is bounded
 by the normal force. This is achieved by scaling the tangential force with 
%
\begin{equation}
\vec{F}_{t_{ij}} \leftarrow
\vec{F}_{t_{ij}}\frac{\|\mu\vec{F}_{n_{ij}}\|_2}{\|\vec{F}_{t_{ij}}\|_2}.
\label{eq7}
\end{equation}
%
%and recomputing the stationary elastic displacement using the scaled force, thus:
This implies a truncation of the elastic displacement, since the Coulomb limit is reached when two spheres slip against each other without
inducing additional deformations. Thus, the deformation is truncated as:
%
\begin{equation}
\vec{u}_{t_{ij}} =
-\frac{1}{k_t}\left(\vec{F}_{t_{ij}} \sqrt{\frac{2R}{\delta_{ij}}} + \gamma_t
m_{\text{eff}}\vec{v}_{t_{ij}}\right)  .
\end{equation}
%
Considering that each particle $i$ interacts with all particles $j$ it is in contact with, the total resultant force on particle $i$ is computed by summing the contributions of all contact pairs $(i,j)$. Including also gravity, we obtain the total force on grain $i$:
%
\begin{equation}
\vec{F}_i^{\text{tot}}=m \vec{g} + \sum_j
\left(\vec{F}_{n_{ij}}+\vec{F}_{t_{ij}}\right)  ,
\end{equation}
%
where $\vec{g}$ is the acceleration due to gravity. 
In the Silbert model, particles also have a rotational degree of freedom. Therefore, the total torque on particle $i$ is calculated as:
%
\begin{equation}
\vec{T}_i^{\text{tot}} = -R \sum_j \vec{n}_{ij}\times\vec{F}_{t_{ij}} \, .
\end{equation}
%
% $\vec{r}_i$ and angular velocities $\vec{\omega}_i$ for each particle $i$ at
% time step $n+1$, 
We integrate the equations of motion 
using the second-order accurate leap frog scheme
%with a time step of $\delta t = 10^{-6}\,$s, viz.:
%
\begin{equation}
   \vec{v}_i^{n+1} = \vec{v}_i^n + \frac{\delta t}{m}\vec{F}_i^{\text{tot}} \, ,
   \qquad
   \vec{r}_i^{n+1} = \vec{r}_i^n + \delta t \vec{v}_i^{n+1}
\end{equation}
\begin{equation}
   \vec{\omega}_i^{n+1} = \vec{\omega}_i^n + \frac{\delta t}{I_i}\vec{T}_i^{\text{tot}} \, ,
\end{equation}
%
where $\vec{r}_i^{n}$, $\vec{v}_i^{n}$, $\vec{\omega}_i^{n}$ denote respectively the position, velocity, and angular velocity of particle $i$ at time step $n$, and $\delta t$ is the time-step size. 

\section{RESULTS}
\label{sec:results}

\begin{figure}[t]
	\centering
	\includegraphics[width=0.98\textwidth]{images/snippet-code-latex.pdf}
	\caption{Excerpts of the PPME code for parallel DEM simulations.}
	\label{fig:PPME-code}
\end{figure}%


% Intro to section
We describe how the above DEM model is implemented in PPME and present the results of a simulation using 82,300 particles. 
% Describe code
Figure~\ref{fig:PPME-code} shows excerpts of the PPME code for the model 
described in Section~\ref{sec:case_study}.
The code for setting up the parallel simulation is in section (1). It defines the external boundary conditions on the computational domain, decomposes the problem onto the available processors, and adds ghost (halo) layers around each processor. 
%Nesrine: Please check text below and adapt to new fig
The particle properties and their datatypes are defined and initialized by the code section (2). Each particle property has a datatype, a dimension, a human-readable name, a numerical precision, a flag stating whether this property is required, and a variable name. 
The remaining code (3-6) shows the implementations of
Equations~\ref{eq2}, \ref{eq3}, \ref{eq5} and \ref{eq7}, respectively.
Equation~\ref{eq2} is an example of native PPME code, using
particle properties. 
Equation~\ref{eq5} is an example of inline Fortran code, enabling access to functions not supported in PPME.
% JCM: {{\color{red}{From Ivo:  Like how to code the main interaction loop, how to do time stepping, how to set up variables, etc.}}. If possible we should include code to reflect what Ivo mentioned. 

% Describe results
We illustrate the PPME implementation using a classical test case for granular flow simulations: an avalanche down an inclined plane. The same test case has also previously been implemented in PPM~\cite{Walther:2007,Walther:2009}, enabling direct comparison of the codes and results. 
We set up the simulation as described~\cite{Walther:2009} and run it using 82,300 particles with $k_n=7.849$, $k_t=2.243$, $\gamma_n=3.401$, $\mu = 1$, $R=0.06$.
All particle masses are set to $m=0.001$ and the gravitational acceleration to $g = 9.81$. The size of the box-shaped domain is fixed to $8.4 \times 3.0 \times 3.18$. 
Initially, the particles are placed on a regular Cartesian grid inside a box of size $4.26 \times 3.06 \times 1.26$. The simulation box is inclined by 30 degrees in the $xz$ plane by appropriately rotating the gravity vector. The $y$-direction of the domain is periodic.
Figure~\ref{fig:dem_sim} visualizes the simulation results at different time points, showing the avalanche down the inclined plane. Color indicates the $x$-component of the particle velocity from low (blue) to high (red). The fixed walls on the bottom, left, and right of the domain are modeled by immobile particles of the same kind. The visualizations are done using ParaView, directly reading the VTK files produced by a single PPME ``print'' statement, illustrating the parallel file I/O amenities of PPME. 

%% DEM simulations figures %%
\begin{figure}[t]
\begin{minipage}[t]{0.32\textwidth}
 \includegraphics[scale=0.15]{images/Dem_0.jpg}
 \subcaption{t = 0 s}
\end{minipage}
\begin{minipage}[t]{0.32\textwidth}
 \includegraphics[scale=0.15]{images/Dem_200.jpg}
 \subcaption{t = 2.0 s}
\end{minipage}
\begin{minipage}[t]{0.32\textwidth}
 \includegraphics[scale=0.15]{images/Dem_400.jpg}
 \subcaption{t = 4.0 s}
\end{minipage}
\caption{Visualization of PPME simulation results of an avalanche down and inclined plane. The panels show the particles at the indicated times. Color from blue to red indicates the $x$-component of the velocity of the particles.} \label{fig:dem_sim}
\end{figure}

% Discussion
%{\color{red}{Nesrine, Pietro: Please check complete, or rewrite}}
%\subsection{Gray-scott reaction-diffusion}
%\subsection{Lennarnd-Jones Potential}
%\subsection{Herzian Contact Model}

\section{CONCLUSIONS}
\label{sec:conclusions}
We presented PPME, an integrated development environment for parallel particle-mesh simulations. PPME is based on a 
complete language model for particle methods. Based on the core model, 
PPME provides an extensible static type system, including physical dimensions. For demonstration purposes,
we exemplarily implemented a classical DEM model in PPME and used it to generate
executable Fortran code that links against the PPM library and can be executed on distributed-memory computers using message passing.
% JCM: Taken out for space reasons.
%Further application examples and a detailed discussion of the domain model as well as the underlying type system 
%can be found in Ref.~\cite{Nett2017}.
In the future, we will extend PPME to support additional abstractions from the particle-mesh domain. We will improve the internal architecture of PPME by adding an additional target-language abstraction layer
in order to be able to support output languages other than Fortran. This will provide a versatile and standard platform for parallel particle methods across a wide range of applications from discrete to continuum simulations.

\section*{ACKNOWLEDGEMENTS}
This work was partly supported by the German Research Foundation (DFG) within the Cluster of Excellence ``Center for Advancing Electronics Dresden''.

\AtEndEnvironment{thebibliography}{
\bibitem{Camborde}  F. Camborde and C. Mariotti and F.V. Donz\'{e} \textit{Numerical study of rock and concrete behaviour by discrete element modelling}. Computers and Geotechnics,
Vol. 27., (2000).
\bibitem{Hentz}  S\'{e}bastien Hentz, Laurent Daudeville, and Fr\'{e}d\'{e}ric V. Donz\'{e} \textit{Numerical study of rock and concrete behaviour by discrete element modelling}. Computers and Geotechnics,
130(6):709–719, (2004).
\bibitem{Shiu}  W. J. Shiu, F. V. Donzé, and L. Daudeville \textit{Compaction process in concrete during missile impact: a dem analysis}. Computers and Concrete,
5(4):329–342, (2008).
\bibitem{Hunt}  Hunt, M.L. \textit{Discrete element simulations for graular material flows: effective thermal
	conductivity and self-diffusivity}. International Journal of Heat and Mass Transfer,
Vol. 40 No. 13, pp. 3059-68, (1997).
\bibitem{Douady1}  Daerr, A. and Douady, S. \textit{Two types of avalanche behaviour in granular media}. Nature,
Vol. 399, pp. 241-3, (1999).
\bibitem{Douady2}  Douady, S., Andreotti, B., Daerr, A. and Clad\'{e} \textit{From a grain to avalanches: on the physics of granular surface flows}. Comptes Rendus Physique,
Vol. 3, pp. 177-87, (2002).
\bibitem{Douady3}  Dutt, M., Hancock, B., Bentham, C. and Elliot, J. \textit{An implementation of granular dynamics for simulating frictional elastic particles based on the DL\_POLY code}. Computer Physics Communications,
Vol. 166, pp. 26-44, (2005).
\bibitem{Gennes}  de Gennes, P.G.  \textit{Granular matter: a tentative view}. Reviews of Modern Physics,
 Vol. 71 No. 2, pp. 374-82, (1999).
\bibitem{Silbert}  Silbert, L.E., Grest, G.S. and Plimpton, S.J. \textit{Boundary effects and self-organization in
dense granular flows}. Physics of Fluids,
 Vol. 14 No. 8, pp. 2637-46, (2002).
\bibitem{Luding}  Stefan Luding \textit{Introduction to discrete element methods
}. European Journal of Environmental and Civil Engineering (2008).
\bibitem{Landry}  Landry, J.W., Grest, G.S., Silbert, L.E. and Plimpton, S.J. \textit{Confined granular packings\: structure, stress, and forces}. Physical Review E,
Vol. 67, p. 041303, (2003).
}
\begin{spacing}{0.8}
\bibliographystyle{plain}
\bibliography{bibliography.bib,online.bib}
\end{spacing}

\end{document}


